from django.urls import path 

from rest_framework.routers import DefaultRouter

from .views import (
    AdmissionProcessViewSet,
    ExamViewSet,
    EvaluationViewSet,
)

router = DefaultRouter()
router.register(r'api/admissionprocess/admissionprocess', AdmissionProcessViewSet, basename="admissionProcess")
router.register(r'api/admissionprocess/exam', ExamViewSet, basename="exam")
router.register(r'api/admissionprocess/evaluation', EvaluationViewSet, basename="evaluation")
urlpatterns = [
]

urlpatterns += router.urls
