from django.shortcuts import render

from rest_framework import viewsets
from rest_framework.permissions import (
    IsAuthenticated,
    IsAdminUser
    )

from django_filters.rest_framework import DjangoFilterBackend

from utils.paginations import CustomPagination

from apps.admissionsProcesses.models import (
    AdmissionProcess,
    Exam,
    Evaluation,
    )

from .serializers import (
    AdmissionProcessSerializer,
    ExamSerializer,
    EvaluationSerializer,
    )

from .filters import ProductFilter
# Create your views here.

class AdmissionProcessViewSet(viewsets.ModelViewSet):
    permission_classes = [
        IsAuthenticated,
        IsAdminUser,
        ]
    model = AdmissionProcess
    serializer_class = AdmissionProcessSerializer
    queryset = AdmissionProcess.objects.filter(
        is_active = True
        )
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['year', 'is_active']

class ExamViewSet(viewsets.ModelViewSet):
    permission_classes = [
        IsAuthenticated,
        IsAdminUser,
        ]
    model = Exam
    serializer_class = ExamSerializer
    queryset = Exam.objects.filter(
        is_active = True
        )
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['admission_process', 'number', 'type', 'is_active']

class EvaluationViewSet(viewsets.ModelViewSet):
    permission_classes = [
        IsAuthenticated,
        IsAdminUser,
        ]
    model = Evaluation
    serializer_class = EvaluationSerializer
    queryset = Evaluation.objects.filter(
        is_active = True
        )
    #pagination_class = CustomPagination
    filter_backends = [DjangoFilterBackend]
    filterset_class = ProductFilter