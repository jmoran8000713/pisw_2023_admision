from rest_framework import serializers

from apps.roundInscriptions.models import (
    RoundInscription,
    UserInscription,
    )

from apps.raffles.models import (
    Raffle,
    RaffleWinner,
    )

from apps.admissionsProcesses.api.serializers import EvaluationSerializer

from apps.users.api.serializers import (
    BasicUserSerializer,
    )

from apps.admissionsProcesses.api.serializers import (
    EvaluationSerializer,
    )

class RoundInscriptionSerializer(serializers.ModelSerializer):

    teachers_enrrolled = serializers.SerializerMethodField()
    is_user_inscripted = serializers.SerializerMethodField()
    is_user_winner = serializers.SerializerMethodField()
    round_was_raffled = serializers.SerializerMethodField()

    is_active = serializers.BooleanField(default=True)

    is_user_with_credential = serializers.SerializerMethodField()
    credential = serializers.SerializerMethodField()

    created_by = BasicUserSerializer(
        default = serializers.CurrentUserDefault(),
        required=False,
    )

    def get_is_user_with_credential(self, obj):
        from apps.credentials.models import Credential
        from apps.credentials.api.serializers import MinimalCredentialSerializer
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
            credential = Credential.objects.filter(
                raffle_winner_assignment__raffle_winner__user_winner = user,
                raffle_winner_assignment__raffle_winner__raffle__round_inscription = obj,
                is_active = True
                ).first()
            
            if credential:
                return True
            
        return False
    
    def get_credential(self, obj):
        from apps.credentials.models import Credential
        from apps.credentials.api.serializers import MinimalCredentialSerializer
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
            credential = Credential.objects.filter(
                raffle_winner_assignment__raffle_winner__user_winner = user,
                raffle_winner_assignment__raffle_winner__raffle__round_inscription = obj,
                is_active = True
                ).first()
            
            if credential:
                return MinimalCredentialSerializer(credential).data
            
        return None
    
    def get_round_was_raffled(self, obj):
        raffle = Raffle.objects.filter(
            round_inscription = obj,
            is_active = True
        ).first()

        if raffle:
            return True
        return False

    def get_is_user_winner(self, obj):
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user

            raffle_winner = RaffleWinner.objects.filter(
                raffle__round_inscription = obj,
                raffle__is_active = True,
                user_winner = user,
                is_active = True,
                ).first()
            
            if raffle_winner:
                return True
        return False    
    
    def get_teachers_enrrolled(self, obj):
        return UserInscription.objects.filter(
            round_inscription = obj,
            is_active = True,
            ).count()
    
    def get_is_user_inscripted(self, obj):
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user

            user_inscription = UserInscription.objects.filter(
                round_inscription = obj,
                user_inscripted = user,
                is_active = True
                ).first()
            
            if user_inscription:
                return True
        return False
    
    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation['evaluation'] = EvaluationSerializer(instance.evaluation).data
        return representation    
    
    class Meta:
        model = RoundInscription
        fields = '__all__'
        extra_kwargs = {
            'created_at': {'read_only': True},
            'updated_at': {'read_only': True},
        }

class UserInscriptionSerializer(serializers.ModelSerializer):
    user_inscripted = BasicUserSerializer(
        default = serializers.CurrentUserDefault(),
        required=False,
    )
    is_active = serializers.BooleanField(default=True)
    is_user_winner = serializers.SerializerMethodField()

    def validate_round_inscription(self, value):
        if value.is_blocked == True:
            raise serializers.ValidationError("Actually is blocked")
        return value
    
    def get_is_user_winner(self, obj):
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user

            raffle_winner = RaffleWinner.objects.filter(
                raffle__round_inscription = obj.round_inscription,
                raffle__is_active = True,
                user_winner = user,
                is_active = True,
                ).first()
            
            if raffle_winner:
                return True
        return False    
    
    def validate(self, data):
        round_inscription = data['round_inscription']
        user_inscripted = data['user_inscripted']

        existing_instance = UserInscription.objects.filter(
            round_inscription=round_inscription,
            user_inscripted=user_inscripted,
            is_active=True
        ).first()

        if existing_instance:
            raise serializers.ValidationError("There is an object with the same round_inscription and user_inscripted already created")
        return data


    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation['round_inscription'] = RoundInscriptionSerializer(instance.round_inscription).data
        return representation    
    
    class Meta:
        model = UserInscription
        fields = '__all__'
        extra_kwargs = {
            'created_at': {'read_only': True},
            'updated_at': {'read_only': True},
        }

class RoundInscriptionShortListSerializer(serializers.ModelSerializer):
    evaluation = EvaluationSerializer()
    class Meta:
        model = RoundInscription
        fields = ['id','evaluation']
