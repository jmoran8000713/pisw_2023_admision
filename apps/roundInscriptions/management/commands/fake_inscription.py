from apps.users.models import User
from apps.roundInscriptions.models import RoundInscription, UserInscription

from django.core.management.base import BaseCommand

class Command(BaseCommand):
    help = 'Inscribir usuarios a una ronda de inscripción'
    
    def add_arguments(self, parser):
        parser.add_argument('round', type=str, help='Indica la ronda a la que se van a inscribir')
        parser.add_argument('amount', type=int, help='Indica el número de inscritos que habrá')
        
    def handle(self, *args, **kwargs):
        round_id = kwargs['round']
        amount = kwargs['amount']

        aaa = RoundInscription.objects.all()
        print(aaa)
        try:
            round_instance = RoundInscription.objects.get(id=round_id)
        except RoundInscription.DoesNotExist:
            self.stdout.write(self.style.ERROR(f'La ronda con el ID {round_id} no existe.'))
            return
        
        # Suponiendo que deseas usar siempre el mismo usuario para inscribir

        
        # Obtener los primeros "amount" usuarios que no estén inscritos en esta ronda
        users_to_inscribe = User.objects.exclude(userinscription__round_inscription=round_instance).order_by('?')[:amount]

        for user in users_to_inscribe:
            # Crear la inscripción de usuario asociada a la ronda y al usuario
            UserInscription.objects.create(
                round_inscription=round_instance,
                user_inscripted=user,
            )

        self.stdout.write(self.style.SUCCESS(f'Se crearon {amount} registros ficticios para la ronda {round_id}.'))

