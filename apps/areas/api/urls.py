from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import AreaViewSet, PavilionViewSet, SchoolViewSet,ClassroomViewSet, EntranceViewSet

router = DefaultRouter()
router.register(r'api/area', AreaViewSet, basename='area')
router.register(r'api/pavilion', PavilionViewSet, basename='pavilion')
router.register(r'api/school', SchoolViewSet, basename='school')
router.register(r'api/classroom', ClassroomViewSet, basename='classroom')
router.register(r'api/entrance', EntranceViewSet, basename='entrance')

urlpatterns = [
    path('', include(router.urls)),
]