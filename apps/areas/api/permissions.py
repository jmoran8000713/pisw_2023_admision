from rest_framework import permissions

class AllowReadAuthenticatedUser(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.user_type == 'ADMIN'

class AllowReadWriteAuthenticatedUser(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.user_type == 'ADMIN' or view.action in ['create', 'update', 'partial_update', 'destroy']
