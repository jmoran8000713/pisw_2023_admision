from django.db import models
from django.utils.translation import gettext_lazy as _

from utils.abstractmodels import BaseModel

class Area(BaseModel):
    name = models.CharField(
        _('name'), 
        max_length=255, 
        blank=False, 
        null=False
        )
    description = models.CharField(
        _('description'), 
        blank=True, 
        null=False
        )
    
class Entrance(BaseModel):
    name = models.CharField(
        _('name'), 
        max_length=255, 
        blank=False, 
        null=False
        )
    description = models.CharField(
        _('description'), 
        blank=True, 
        null=False
        )
    area = models.ForeignKey(
        Area, 
        verbose_name=_('area'), 
        on_delete=models.CASCADE, 
        blank=False, 
        null=False
        )

    def __str__(self):
        return self.name
    

class School(BaseModel):
    name = models.CharField(
        _('name'), 
        max_length=255, 
        blank=False, 
        null=False
        )
    description = models.CharField(
        _('description'), 
        blank=True, 
        null=False
        )
    area = models.ForeignKey(
        Area, 
        on_delete=models.CASCADE, 
        blank=False, 
        null=False,
        verbose_name=_('area')
        )

    def __str__(self):
        return self.name

class Pavilion(BaseModel):
    name = models.CharField(
        _('name'), 
        max_length=255, 
        blank=False, 
        null=False
        )
    description = models.CharField(
        _('description'), 
        blank=True, 
        null=False
        )
    school = models.ForeignKey(
        School, 
        on_delete=models.CASCADE, 
        blank=False, 
        null=False,
        verbose_name=_('school')
        )

    def __str__(self):
        return self.name
    
class Classroom(BaseModel):

    name = models.CharField(
        _('name'), 
        max_length=255, 
        blank=False, 
        null=False
        )
    description = models.CharField(
        _('description'), 
        blank=True, 
        null=False
        )
    rows = models.PositiveIntegerField(
        _('rows'), 
        blank=True, 
        null=True
        )
    columns = models.PositiveIntegerField(
        _('columns'), 
        blank=True, 
        null=True
        )
    pavilion = models.ForeignKey(
        Pavilion, 
        on_delete=models.CASCADE, 
        blank=False, 
        null=False,
        verbose_name=_('pavilion')
        )
    def __str__(self):
        return f'Classroom {self.name}' if self.name is not None else 'Classroom without name'