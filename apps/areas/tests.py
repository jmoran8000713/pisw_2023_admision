from django.test import TestCase
from django.utils import timezone
from .models import Area, Pavilion, School, Classroom, Entrance

class ModelTestCase(TestCase):

    def setUp(self):
        # Crea instancias de modelos para usar en las pruebas
        self.area = Area.objects.create(name='Área de Prueba', description='Descripción de prueba')
        self.pavilion = Pavilion.objects.create(name='Pabellón de Prueba', description='Descripción de prueba')
        self.school = School.objects.create(name='Escuela de Prueba', description='Descripción de prueba', area=self.area)
        self.classroom = Classroom.objects.create(number=101, description='Aula de Prueba', capacity=30)
        self.entrance = Entrance.objects.create(name='Entrada de Prueba', area=self.area)

    def test_models_and_fields(self):
        # Verifica que las instancias de modelos se hayan creado correctamente
        self.assertEqual(Area.objects.count(), 1)
        self.assertEqual(Pavilion.objects.count(), 1)
        self.assertEqual(School.objects.count(), 1)
        self.assertEqual(Classroom.objects.count(), 1)
        self.assertEqual(Entrance.objects.count(), 1)

        # Verifica que los campos de los modelos tengan los valores esperados
        self.assertEqual(self.area.name, 'Área de Prueba')
        self.assertEqual(self.pavilion.name, 'Pabellón de Prueba')
        self.assertEqual(self.school.name, 'Escuela de Prueba')
        self.assertEqual(self.classroom.number, 101)
        self.assertEqual(self.entrance.name, 'Entrada de Prueba')

        # Modifica un campo y verifica la actualización
        self.area.name = 'Nueva Área de Prueba'
        self.area.save()
        self.assertEqual(Area.objects.get(pk=self.area.pk).name, 'Nueva Área de Prueba')

        # Elimina un modelo y verifica que se haya eliminado correctamente
        self.classroom.delete()
        self.assertEqual(Classroom.objects.count(), 0)
