from django.urls import path 

from rest_framework.routers import DefaultRouter

from .views import (
    IncidentViewSet,
    UserIncidentViewSet,
)

router = DefaultRouter()
router.register(r'api/incident/incident', IncidentViewSet, basename="incident")
router.register(r'api/incident/userincident', UserIncidentViewSet, basename="userIncident")

urlpatterns = [

]

urlpatterns += router.urls
