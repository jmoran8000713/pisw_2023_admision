from django.db import models

from utils.abstractmodels import BaseModel

class Coordination(BaseModel):
    
    name = models.CharField(
        max_length=255,
        blank=False,
        null=False,
        verbose_name = 'Nombre de la Coordinacion',
        )
    
    description = models.CharField(
        blank=True,
        null=False,
        verbose_name = 'Descripción de la Coordinacion'
        )