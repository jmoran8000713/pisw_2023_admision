import os
import django
from faker import Faker
from apps.users.models import User, UserType

from django.core.management.base import BaseCommand
from django.utils.crypto import get_random_string

fake = Faker()

class Command(BaseCommand):
    help = 'Genera datos ficticios para tu aplicación'
    
    def add_arguments(self, parser):
        parser.add_argument('total', type=int, help='Indica el número de registros a crear')

    def generate_unsa_email(self):
        # Genera un correo electrónico falso con Faker y lo modifica para que termine en "@unsa.edu.pe"
        email = fake.email()
        return email.split('@')[0] + '@unsa.edu.pe'
    
    def handle(self, *args, **kwargs):
        total = kwargs['total']

        for _ in range(total):
            # Crea instancias de tu modelo con datos ficticios
            User.objects.create(
                first_name=fake.first_name(),
                last_name = fake.last_name() + ' ' + fake.last_name(),
                work=fake.job(),
                email=self.generate_unsa_email(),
                user_type=UserType.TEACHER,
                dni=fake.numerify(text='##########')
                # Agrega más campos según tu modelo
            )

        self.stdout.write(self.style.SUCCESS(f'Se crearon {total} registros ficticios.'))
