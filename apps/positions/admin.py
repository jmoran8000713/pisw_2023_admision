from django.contrib import admin

from .models import (
    Position,
    )

class PositionAdminConfig(admin.ModelAdmin):
    model = Position

    list_display = (
        'id',
        'name',
        'description',
        'is_active',
        'created_at',
        'updated_at'
        )

    ordering = ('-created_at',)

admin.site.register(Position, PositionAdminConfig)
