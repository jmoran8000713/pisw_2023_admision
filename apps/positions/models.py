from django.db import models
from django.utils.translation import gettext_lazy as _

from utils.abstractmodels import BaseModel

class Position(BaseModel):
    name = models.CharField(
        max_length=50,
        blank=False,
        null=False,
        verbose_name = _("nombre de la posicion"),
        )

    description = models.CharField(
        max_length=255,
        blank=True,
        null=False,
        verbose_name = _("descripción de la posicion"),
        )  