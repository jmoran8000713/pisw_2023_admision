from django.db import models

from apps.roundInscriptions.models import RoundInscription
from apps.users.models import User

from utils.abstractmodels import BaseModel


class Raffle(BaseModel):

    raffler_user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        )

    number_of_winners = models.PositiveBigIntegerField(
        blank=False,
        null=False,
        )    

    round_inscription = models.ForeignKey(
        RoundInscription,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        )
    
    def delete(self, using=None, keep_parents=False):
        self.is_active = False
        RaffleWinner.objects.filter(
            raffle = self,
        ).update(
            is_active = False,
        )
        self.save()

        
class RaffleWinner(BaseModel):

    raffle = models.ForeignKey(
        Raffle,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        )
    
    user_winner = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        )