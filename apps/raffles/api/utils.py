from django.db.models import Sum, Value
from django.db.models.functions import Coalesce, Greatest
from django.db.models import F, ExpressionWrapper, fields


from apps.raffles.models import Raffle
from apps.roundInscriptions.models import UserInscription
from apps.raffles.models import RaffleWinner

from random import sample

def create_raffle_with_criteria(validated_data):
    round_inscription = validated_data.get('round_inscription')
    number_of_winners = validated_data.get('number_of_winners')

    previous_winners = RaffleWinner.objects.filter(
        raffle__round_inscription=round_inscription,
        raffle__is_active=True
    ).values_list('user_winner', flat=True)

    eligible_users = UserInscription.objects.filter(
        round_inscription=round_inscription,
        is_active=True
    ).exclude(user_inscripted__in=previous_winners)

    # eligible_users_with_sum = eligible_users.annotate(
    #     incidents_sum=Coalesce(
    #         Sum('user_inscripted__userincident__incident__level__value'), 
    #         Value(0)
    #         )
    #     )

    #max_sum = eligible_users_with_sum.order_by('-incidents_sum').first().incidents_sum
    base_value = 2

    # weighted_results = eligible_users_with_sum.annotate(
    #     weight=ExpressionWrapper(
    #         Greatest(base_value - F('incidents_sum'),1),
    #         output_field=fields.IntegerField()
    #         )
    #     )
    
    # for objeto in weighted_results:
    #     print(f"Tu columna: {objeto.weight}")



    winners = sample(list(eligible_users), number_of_winners)

    raffle = Raffle(**validated_data)
    raffle.save()

    for user_inscription in winners:
        RaffleWinner.objects.create(raffle=raffle, user_winner=user_inscription.user_inscripted)

    return raffle