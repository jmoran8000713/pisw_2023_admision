from django.urls import path 

from rest_framework.routers import DefaultRouter

from .views import (
    RaffleViewSet,
    RaffleWinnerViewSet,
)

router = DefaultRouter()
router.register(r'api/raffle/raffle', RaffleViewSet, basename="raffle")
router.register(r'api/raffle/rafflewinner', RaffleWinnerViewSet, basename="raffleWinner")

urlpatterns = [

]

urlpatterns += router.urls
