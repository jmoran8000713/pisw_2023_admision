from django.contrib import admin

from .models import (
    Raffle,
    RaffleWinner,
)


class RoundInscriptionAdminConfig(admin.ModelAdmin):
    model = Raffle

    list_display = (
        'id',
        'raffler_user',
        'number_of_winners',
        'round_inscription',
        'created_at',
        'updated_at'
        )

    ordering = ('-created_at',)

class UserInscriptionAdminConfig(admin.ModelAdmin):
    model = RaffleWinner

    list_display = (
        'id',
        'raffle',
        'user_winner',
        'created_at',
        'updated_at'
        )

    ordering = ('-created_at',)

admin.site.register(Raffle,RoundInscriptionAdminConfig)
admin.site.register(RaffleWinner,UserInscriptionAdminConfig)
