import uuid
from django.test import TestCase
from apps.roundInscriptions.models import RoundInscription
from apps.users.models import User
from .models import Raffle, RaffleWinner

class RaffleModelTest(TestCase):
    def setUp(self):
        # Create test data for models
        self.raffler_user = User.objects.create(email='raffler@example.com', user_type='RAFFLER', password='password123')
        self.round_inscription = RoundInscription.objects.create(name='Test Round', description='Test Description')

        self.raffle_data = {
            'raffler_user': self.raffler_user,
            'number_of_winners': 3,
            'round_inscription': self.round_inscription,
            'is_active': True,
        }

    def test_raffle_model(self):
        raffle = Raffle.objects.create(**self.raffle_data)
        self.assertEqual(str(raffle), str(raffle.id))
        self.assertEqual(raffle.raffler_user, self.raffler_user)
        self.assertEqual(raffle.number_of_winners, self.raffle_data['number_of_winners'])
        self.assertEqual(raffle.round_inscription, self.round_inscription)
        self.assertTrue(raffle.is_active)

class RaffleWinnerModelTest(TestCase):
    def setUp(self):
        # Create test data for models
        self.raffler_user = User.objects.create(email='raffler@example.com', user_type='RAFFLER', password='password123')
        self.round_inscription = RoundInscription.objects.create(name='Test Round', description='Test Description')
        self.raffle = Raffle.objects.create(raffler_user=self.raffler_user, number_of_winners=3, round_inscription=self.round_inscription, is_active=True)
        self.user_winner = User.objects.create(email='winner@example.com', user_type='WINNER', password='password123')

        self.raffle_winner_data = {
            'raffle': self.raffle,
            'user_winner': self.user_winner,
            'is_active': True,
        }

    def test_raffle_winner_model(self):
        raffle_winner = RaffleWinner.objects.create(**self.raffle_winner_data)
        self.assertEqual(str(raffle_winner), str(raffle_winner.id))
        self.assertEqual(raffle_winner.raffle, self.raffle)
        self.assertEqual(raffle_winner.user_winner, self.user_winner)
        self.assertTrue(raffle_winner.is_active)