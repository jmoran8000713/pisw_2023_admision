# Aplicación Django con Docker y PostgreSQL

Este proyecto es una aplicación Django en Docker que utiliza PostgreSQL como su base de datos. Esta configuración proporciona un entorno de desarrollo consistente en diferentes plataformas, haciendo que el proceso de desarrollo sea más fluido y con menos errores.

## Descripción

Este proyecto consiste en una aplicación Django que se ejecuta dentro de un contenedor Docker. Esta aplicación Django utiliza una base de datos PostgreSQL, que también se ejecuta dentro de un contenedor Docker. Los detalles de la configuración del proyecto se pueden encontrar en el archivo `docker-compose.yml` en la raíz del proyecto.   

## Instalación

### Prerrequisitos

1. [Docker](https://docs.docker.com/get-docker/): La plataforma base utilizada para crear contenedores aislados para tus aplicaciones.

2. [Docker Compose](https://docs.docker.com/compose/install/): Una herramienta para definir y gestionar aplicaciones Docker de múltiples contenedores.

3. [Git](https://git-scm.com/downloads): Se utiliza para clonar el repositorio del proyecto. Si no lo tienes instalado, puedes descargarlo desde el enlace. En Ubuntu se puede usar el comando `sudo apt install git`.

### Instrucciones de instalación

1. **Clonar el repositorio**: Clona el repositorio de GitLab en tu máquina local utilizando el siguiente comando en tu terminal:
  ```
git clone https://gitlab.com/frego100/pisw_2023_admision.git
  ```

2. **Navegar al directorio del proyecto**: Después de clonar el repositorio, navega al directorio del proyecto:
- En Linux:
  ```
  cd ~/Directorio_donde_guardaste/pisw_2023_admision/backend-admision
  ```
- En Windows (powershell o cmd):
  ```
  cd Directorio_donde_guardaste\pisw_2023_admision\backend-admision
  ```

Aquí encontrarás los archivos llamados Dockerfile, docker-compose.yml, requirements.txt y manage.py.

3. **Configurar las variables de entorno**: Cambie el nombre de `env.example` a `.env` en el directorio raíz del proyecto. Abra el archivo y   personalize las variables que se le piden editar.

4. **Construir y ejecutar las imágenes Docker**: Ejecuta el siguiente comando en tu terminal:
  ```
docker-compose --file docker-compose.yml up --build
  ```

5. **Migrar la base de datos de Django**: Este paso inicializa el esquema de tu base de datos. En una nueva ventana de terminal, ejecuta los siguientes comandos: 
Para Linux:
  ```
docker exec -it "admision_unsa_dev" bash
python manage.py makemigrations users
python manage.py migrate
  ```
Para encontrar el nombre de los contenedores en ejecución, usa el comando `docker ps`. La columna NAMES muestra los nombres de los contenedores.

##Uso

**Acceder a la aplicación Django**: Sigue estos pasos:
- Abre tu navegador web.
- En la barra de direcciones, ingresa "localhost:8000" y presiona Enter.
- Esto te llevará a la página principal de la aplicación Django en tu entorno local.

Una vez que hayas implementado tu aplicación Django en un servidor, sigue estos pasos para acceder a ella:
- La dirección IP del servidor en el que está alojada la aplicación Django.
- Abre tu navegador web.
- En la barra de direcciones, ingresa la dirección IP del servidor seguida de ":8000" y presiona Enter.
- Por ejemplo: "192.168.1.100:8000"
- Esto te llevará a la página principal de la aplicación Django en el servidor remoto.

**Visualizar la base de datos**: Para visualizar la base de datos de tu aplicación Django, puedes utilizar DBeaver. Sigue estos pasos:
- Descarga e instala DBeaver en tu computadora desde el [sitio web oficial](https://dbeaver.io/).
- Abre DBeaver y crea una nueva conexión de base de datos.
- Selecciona el tipo de base de datos correspondiente a la aplicación Django (por ejemplo PostgreSQL, SQLite, etc.).
- Proporciona los detalles de conexión necesarios, como la dirección IP del servidor de la base de datos, el puerto (por defecto, el puerto 5432 para PostgreSQL), el nombre de usuario, la contraseña y el nombre de la base de datos.
- Si estás trabajando en tu entorno local, utiliza "localhost" como la dirección IP y "5432" como el puerto.
- Si estás accediendo a la base de datos de forma remota, utiliza la dirección IP del servidor con el puerto 5432.
- En un servidor, utiliza la dirección IP del servidor con el puerto 5432.
- Haz clic en "Conectar" para establecer la conexión con la base de datos.
- Una vez conectado, podrás explorar y visualizar los datos de la base de datos de tu aplicación Django utilizando la interfaz de DBeaver.

![Imagen de la aplicación](https://drive.google.com/file/d/1j5cfyUfv5aY2oCp_-Gn_iIy-mCGIEmwR/view?usp=sharing)
![Imagen de la aplicación](url-a-la-imagen-de-la-aplicación)

## Verificando las imágenes Docker

Para ver una lista de las imágenes Docker, usa el comando `docker images`. Esto te mostrará todas las imágenes almacenadas en tu sistema junto con sus respectivos detalles.

Para ver los contenedores en ejecución, usa el comando `docker ps`. Para ver todos los contenedores, no solo los que están actualmente en ejecución, añade la bandera `-a` al comando: `docker ps -a`.

## Más comandos de Docker

- `docker rm nombre-del-contenedor`: Elimina un contenedor Docker.
- `docker rmi nombre-de-la-imagen`: Elimina una imagen Docker.
- `docker stop nombre-del-contenedor`: Detiene un contenedor Docker en ejecución.


## Soporte

Si necesitas ayuda con este proyecto, puedes encontrar más información en:

- [Documentación del proyecto](https://link-to-project-documentation)
- [Abrir un issue](https://link-to-issue-tracker)
- [Comunidad de Slack](https://link-to-slack-community)

## Hoja de Ruta

- [ ] Mejorar la interfaz de usuario
- [ ] Agregar funcionalidad de autenticación de usuarios
- [ ] Optimizar rendimiento de la base de datos

## Contribuciones

¡Las contribuciones son bienvenidas! Si deseas contribuir a este proyecto, sigue los siguientes pasos:

1. Haz un fork del repositorio.
2. Crea una rama nueva para tus cambios.
3. Realiza los cambios en tu rama.
4. Envía una pull request con una descripción detallada de tus cambios.

## Autores y Reconocimientos

Agradecemos a todas las personas que han contribuido a este proyecto.

- Chaco Huamani, Alex
  - Email: achacohu@unsa.edu.pe

- Castillo Rojas, Jairo Miguel
  - Email: jcastillo@unsa.edu.pe

- Tito Durand, Rudy Roberto
  - Email: rtitod@unsa.edu.pe

- Guevara Gutierrez, Marcelo Andre
  - Email: mguevarag@unsa.edu.pe

- Ilachoque Hanccoccallo, Christian Wilfredo
  - Email: cilachoque@unsa.edu.pe

- Luque Sanabria, Felman
  - Email: fluques@unsa.edu.pe

- Moran Fuño, Jeampier Anderson
  - Email: jmoran@unsa.edu.pe

- Delgado Huacallo, Renato Eduardo
  - Email: rdelgadoh@unsa.edu.pe


## Licencia

Este proyecto está bajo la licencia [nombre de la licencia]. Consulta el archivo `LICENSE` para obtener más información.

## Estado del Proyecto

El desarrollo de este proyecto se encuentra en curso y estamos abiertos a nuevas contribuciones y mejoras. Si tienes alguna pregunta o inquietud, no dudes en comunicarte con nosotros.

