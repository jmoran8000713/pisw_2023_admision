"""
URL configuration for admision_unsa project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import (
    path,
    include,
    )
from django.conf.urls.static import static
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from django.conf import settings



schema_view = get_schema_view(
    openapi.Info(
        title="Documentation API",
        default_version="v0.1",
        description="Documentation - People System",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="achaco@unsa.edu.pe"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=[permissions.AllowAny],
)

urlpatterns = [
    path('api/admin/', admin.site.urls),
    #path('', include('apps.users.api.urls')),
    path('', include('apps.admissionsProcesses.api.urls')),
    path('', include('apps.raffles.api.urls')),
    path('', include('apps.roundInscriptions.api.urls')),
    path('', include('apps.areas.api.urls')),
    path('', include('apps.incidents.api.urls')),
    path('', include('apps.assignments.api.urls')),
    path('', include('apps.coordinations.api.urls')),
    path('', include('apps.credentials.api.urls')),
    
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

#path("api/api-auth/", include("rest_framework.urls", namespace="rest_framework")),
# Swagger and redoc documentation
urlpatterns += [
    path(
        "api/swagger/",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    path(
        "api/redoc/", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"
    ),
] 
