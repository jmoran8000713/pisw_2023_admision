# Django Application with Docker and PostgreSQL

This project is a Django application running on Docker that uses PostgreSQL as its database. This configuration provides a consistent development environment across different platforms, making the development process smoother and less error-prone.

## Description

This project consists of a Django application running within a Docker container. This Django application utilizes a PostgreSQL database, which also runs inside a Docker container. Project configuration details can be found in the `docker-compose.yml` file at the root of the project.

## Installation

### Prerequisites

1. [Docker](https://docs.docker.com/get-docker/): The foundational platform used to create isolated containers for your applications.

2. [Docker Compose](https://docs.docker.com/compose/install/): A tool for defining and managing Docker applications with multiple containers.

3. [Git](https://git-scm.com/downloads): Used to clone the project repository. If not installed, you can download it from the link. On Ubuntu, you can use the command `sudo apt install git`.

### Installation Instructions

1. **Clone the repository**: Clone the GitLab repository to your local machine using the following command in your terminal:
   ```
   git clone https://gitlab.com/frego100/pisw_2023_admision.git
   ```

2. **Navigate to the project directory**: After cloning the repository, navigate to the project directory:
   - On Linux:
     ```
     cd ~/Directory_where_you_saved/pisw_2023_admision/backend-admision
     ```
   - On Windows (powershell or cmd):
     ```
     cd Directory_where_you_saved\pisw_2023_admision\backend-admision
     ```

   Here you will find files named Dockerfile, docker-compose.yml, requirements.txt, and manage.py.

3. **Configure environment variables**: Change the name of `env.example` to `.env` in the root directory of the project. Open the file and customize the variables as prompted.

4. **Build and run Docker images**: Execute the following command in your terminal:
   ```
   docker-compose --file docker-compose.yml up --build
   ```

5. **Migrate Django database**: This step initializes your database schema. In a new terminal window, run the following commands:
   For Linux:
   ```
    docker exec -it "admision_unsa_dev" bash
    python manage.py makemigrations users
    python manage.py migrate
   ```
   To find the names of running containers, use the `docker ps` command. The NAMES column shows the container names.

## Usage

**Access Django application**: Follow these steps:
- Open your web browser.
- In the address bar, enter "localhost:8000" and press Enter.
- This will take you to the main page of the Django application in your local environment.

Once you have deployed your Django application on a server, follow these steps to access it:
- The IP address of the server hosting the Django application.
- Open your web browser.
- In the address bar, enter the server's IP address followed by ":8000" and press Enter.
- For example: "192.168.1.100:8000"
- This will take you to the main page of the Django application on the remote server.

**View the database**: To visualize your Django application's database, you can use DBeaver. Follow these steps:
- Download and install DBeaver on your computer from the [official website](https://dbeaver.io/).
- Open DBeaver and create a new database connection.
- Select the database type corresponding to the Django application (e.g., PostgreSQL, SQLite, etc.).
- Provide the necessary connection details, such as the database server's IP address, port (default is 5432 for PostgreSQL), username, password, and database name.
- If you are working in your local environment, use "localhost" as the IP address and "5432" as the port.
- If you are accessing the database remotely, use the server's IP address with port 5432.
- On a server, use the server's IP address with port 5432.
- Click "Connect" to establish a connection to the database.
- Once connected, you can explore and visualize the data in your Django application's database using DBeaver's interface.

![Application Image](url-to-application-image)

## Checking Docker Images

To see a list of Docker images, use the `docker images` command. This will show all images stored on your system along with their respective details.

To view running containers, use the `docker ps` command. To see all containers, not just the ones currently running, add the `-a` flag to the command: `docker ps -a`.

## More Docker Commands

- `docker rm container-name`: Deletes a Docker container.
- `docker rmi image-name`: Deletes a Docker image.
- `docker stop container-name`: Stops a running Docker container.

## Support

If you need assistance with this project, you can find more information at:

- [Project Documentation](https://link-to-project-documentation)
- [Open an Issue](https://link-to-issue-tracker)
- [Slack Community](https://link-to-slack-community)

## Roadmap

- [ ] Improve the user interface
- [ ] Add user authentication functionality
- [ ] Optimize database performance

## Contributions

Contributions are welcome! If you want to contribute to this project, follow these steps:

1. Fork the repository.
2. Create a new branch for your changes.
3. Make your changes in your branch.
4. Submit a pull request with a detailed description of your changes.

## Authors and Acknowledgments

We appreciate all individuals who have contributed to this project.

- Chaco Huamani, Alex
  - Email: achacohu@unsa.edu.pe

- Castillo Rojas, Jairo Miguel
  - Email: jcastillo@unsa.edu.pe

- Tito Durand, Rudy Roberto
  - Email: rtitod@unsa.edu.pe

- Guevara Gutierrez, Marcelo Andre
  - Email: mguevarag@unsa.edu.pe

- Ilachoque Hanccoccallo, Christian Wilfredo
  - Email: cilachoque@unsa.edu.pe

- Luque Sanabria, Felman
  - Email: fluques@unsa.edu.pe

- Moran Fuño, Jeampier Anderson
  - Email: jmoran@unsa.edu.pe

- Delgado Huacallo, Renato Eduardo
  - Email: rdelgadoh@unsa.edu.pe

## License

This project is under the [license name]. Refer to the `LICENSE` file for more information.

## Project Status

The development of this project is ongoing, and we are open to new contributions and improvements. If you have any questions or concerns, feel free to reach out to us.
