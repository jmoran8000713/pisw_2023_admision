import uuid as uuid_lib
from django.db import models
from django.utils.translation import gettext_lazy as _

class BaseModel(models.Model):
    
    id = models.UUIDField(
        primary_key=True,
        default=uuid_lib.uuid4,
        editable=False,
        unique=True,
        db_index=True
        )
    
    created_at = models.DateTimeField(
        _("created datetime"),
        auto_now_add=True,
        blank=False,
        null=False
        )

    updated_at = models.DateTimeField(
        _("updated datetime"),
        auto_now=True,
        blank=False,
        null=False
        )
    
    is_active = models.BooleanField(
        default=True
        )
    
    def delete(self, using=None, keep_parents=False):
        self.is_active = False
        self.save()

    class Meta:
        abstract = True